example-nrf5-gatt-mrt
============================

This is an example project demonstrating the of the `mrt-ble <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-ble.html>`_ tool to create a gatt server on the NRF5 platform. The `mrt-ble` tool allows the user to define a GATT profile with standard and custom services using a simple yaml file. ``mrt-ble`` can use the yaml file to generate code and documentation for the profile and services. 

Along with the code and documentation, ``mrt-ble`` also generates a `Live ICD` which is a single page web app that uses the Web Bluetooth API to connect to the device and interact with the services. The Live ICD can be used to test the device and profile without needing to write a custom application. 

.. image:: https://mrt.readthedocs.io/en/latest/_images/live_icd.png
    :width: 80%
    :alt: Live ICD


.. note: while this can be done in any IDE you prefer, some of notes in the walkthrough assume the project is opened in VS Code.

Building
--------

This project contains a devcontainer configuration file which will build a container with all of the build dependencies. To use this, open the folder in VS Code, click the 'Remote Window' icon in the bottom left corner, and select 'Reopen in Container'. 

.. note:: This requires the machine to have Docker installed, and the VS Code 'Dev Containers' extension 

.. code:: bash 
    
    make

Flashing
--------

If building on a linux host (or in the devcontainer with access to host devices) you can flash with make: 

.. code:: bash 
    
    make flash_softdevice
    make flash


If you are using a windows machine as host, you can flash using the NRF-Connect programmer app.

Update Profile
--------------

The gatt profile can be changed by editing the example_profile.yml file, and then using mrt-ble: 

.. code:: bash 

    mrt-ble -i example_profile.yml -o src/gatt/ -d .

For convenience this has been added as a target to the makefile 

.. code:: bash 

    make profile


