NRF5 Gatt Example 
==================

This project contains the firmware for the nRF5 Gatt Example. In the example we use the `mrt-ble <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-ble.html>`_ tool to generate a BLE GATT Profile. The profile is for a smart sprinkler system which utilizes both custom and standard BLE services. The example is designed to run on the `nRF52840 <https://www.nordicsemi.com/Software-and-Tools/Development-Kits/nRF52840-DK>`_ development kit.

.. toctree::
    :caption: Overview
    :maxdepth: 3
    
    pages/system
    pages/development

.. toctree::
    :caption: Appendix
    :titlesonly:
    :maxdepth: 1
    
    assets/icds/example_gatt_icd
    pages/live_icd
    pages/ota/update_process
