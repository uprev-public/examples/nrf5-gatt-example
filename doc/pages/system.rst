System Overview
===============

The system consists of a single embedded device with a temperature/humidity sensor, a soild moisture sensor, and output terminals that can be connected to remote valves for an irrigation system.

The system is controlled over BLE using a mobile device 


.. figure:: ../assets/diagrams/system.dio.png



Memory Map 
----------

.. figure:: ../assets/diagrams/memorymap.dio.png
