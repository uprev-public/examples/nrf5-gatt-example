Live ICD 
========

This project contains a 'Live ICD' which is a single page web app that can be used to interact with the device. This app is generated automatically by the `mrt-ble <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-ble.html>`_ tool.


.. only:: html

    `Live ICD <../_static/example_live_icd.html>`_

.. only:: latex

    The Live ICD is available at `https://uprev.gitlab.io/nrf5-gatt-example/_staic/example_live_icd.html`