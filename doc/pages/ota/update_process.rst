OTA Process
===========

The update process starts with a version check where a host device (such as a phone) checks the firmware version, and then checks the 'FieldOps' API to make sure that version is the latest for the device. If a newer version is available, the host will download the new image, transfer it to the device, and then apply the update.

.. warning::
    The OTA Update process and bootloader are not implemented in this project. This page documents a proposed process for the update process, and is meant to be an example of how the bootloader and update process could be implemented.

Version Check 
~~~~~~~~~~~~~

This project uses the OTA module from MrT which sets up basic partitions on the memory devices in the system. A host can connect over bluetooth and check the version and serial number of the device. The host can then check the 'FieldOps' API to see if there is a newer version available.

.. uml:: ../../assets/diagrams/ota-discovery.puml


Transfer
~~~~~~~~

The host can then initiate a tranfser and send over the firmware image 

.. uml:: ../../assets/diagrams/ota-xfer.puml

Apply 
~~~~~

Once the transfer is complete, the host can send a 'RESET' command to the device to reboot and start the bootloader. The bootloader will then copy the new firmware image from the staging bank to the program flash memory. It will then set the `OTA_PARTITION_FLAG_NEW_IMAGE` flag on the recovery partitions. The application will clear these flags upon a successful selft test

.. uml:: ../../assets/diagrams/ota-apply.puml

Self Test 
~~~~~~~~~

The application fimrware will perform a self test to verify that the peripherals required for an update are working. If it passes the test, it will clear the 'NEW_IMAGE' flag on the recovery partitions to prevent the recovery images from being applied on next boot. 

.. uml:: ../../assets/diagrams/ota-selftest.puml

Bootloader Process 
------------------


The bootloader copies the updates from the stagining bank to the appropriate memory location (program flash for firmware, and configuration memory for fpga fabric). After the update is complete, the device will start the FPGA and boot to the main application firmware. 

.. mermaid:: ../../assets/diagrams/bootloader.mmd

.. uml:: ../../assets/diagrams/bootloader.puml




